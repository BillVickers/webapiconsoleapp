﻿using System.Collections.Generic;

namespace OlympicsArticles
{
    public class Article
    {      
        public string Headline { get; set; }
        public string ArticleURL { get; set; }
        public string PublishDate { get; set; }
        public string Source { get; set; }




    }
}
