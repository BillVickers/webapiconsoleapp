﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace OlympicsArticles
{
   
    public class Program
    {
       
        static void Main()
        {
            //Call RunAsync then block until completion for console app
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var client = new HttpClient())
            {
                //Localhost is consuming json data
                client.BaseAddress = new Uri("http://localhost:9000/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try {
                    //HTTP GET, send request to API
                    HttpResponseMessage response = await client.
                        GetAsync("https://api.nytimes.com/svc/search/v2/articlesearch.json?query=olympics?fields=headline,web_url,pub_date,source&begin_date=20160101&end_date=20160812&api-key=84d9cdc33a704658a1d51310f76645f8");

                    //Connection error check
                    if (response.StatusCode != HttpStatusCode.OK)throw new Exception(String.Format("Server error (HTTP {0}: {1}).",response.ReasonPhrase,
                        response.RequestMessage));
                        {
                        //Read data
                        string json = await response.Content.ReadAsStringAsync();
                        
                        //Parse data
                        JObject parse = JObject.Parse(json);

                        //New Article object
                        Article article = new Article();

                        //Assign data to object
                        article.Headline = (string)parse["response"]["docs"][0]["headline"]["main"];
                        article.ArticleURL = (string)parse["response"]["docs"][0]["web_url"];
                        article.PublishDate = (string)parse["response"]["docs"][0]["pub_date"];
                        article.Source = (string)parse["response"]["docs"][0]["source"];

                        //Print data                                     
                        Console.WriteLine("Headline: " + article.Headline);
                        Console.WriteLine("Article URL: " + article.ArticleURL);
                        Console.WriteLine("Publish Date: " + article.PublishDate);
                        Console.WriteLine("Source: " + article.Source);
                        Console.WriteLine();

                        //New Article object
                        Article article1 = new Article();

                        //Assign data to object
                        article1.Headline = (string)parse["response"]["docs"][1]["headline"]["main"];
                        article1.ArticleURL = (string)parse["response"]["docs"][1]["web_url"];
                        article1.PublishDate = (string)parse["response"]["docs"][1]["pub_date"];
                        article1.Source = (string)parse["response"]["docs"][1]["source"];

                        //Print data                                     
                        Console.WriteLine("Headline: " + article1.Headline);
                        Console.WriteLine("Article URL: " + article1.ArticleURL);
                        Console.WriteLine("Publish Date: " + article1.PublishDate);
                        Console.WriteLine("Source: " + article1.Source);
                        Console.ReadLine();
                        
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                }
                
            }
        }
    }
}
